var express = require('express'),
app = express(),
port = process.env.PORT || 3000;

var path = require('path');

var requestjson = require('request-json');
var urlRaizMLab = "https://api.mlab.com/api/1/databases/mtecolapa/collections";
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var clienteMLabRaiz;

var urlUsuarios="https://api.mlab.com/api/1/databases/mtecolapa/collections/Usuarios?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var usuarioMLab = requestjson.createClient(urlUsuarios);

var urlClientes="https://api.mlab.com/api/1/databases/mtecolapa/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var usuarioMLabClientes = requestjson.createClient(urlClientes);

var urlAlertas="https://api.mlab.com/api/1/databases/mtecolapa/collections/Alertas?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";
var usuarioMLabAlertas = requestjson.createClient(urlAlertas);



//nombre variable
var bodyParser = require('body-parser');
//Indica que la aplicacion utilizara body tipo json
app.use(bodyParser.json());
//funcion para aceptar peticiones no seguras http
app.use(function(req,res, next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var movimientosJSON = require('./movimientosv2.json');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

//request get que direcciona a un fichero html
app.get('/', function (req, res){
  res.sendFile(path.join(__dirname,'index.html'));
})
//request con parametros
app.get('/clientes/:idCliente', function (req, res){
  res.send('Aqui tienes al cliente número: '+ req.params.idCliente);
})

app.post('/', function (req, res){
  res.send('Hemos recibo su petición post');
})

app.put('/', function (req, res){
  res.send('Hemos recibo su petición cambiada');
})

app.delete('/', function (req, res){
  res.send('Hemos recibo su petición delete');
})

//regresa un archivo json
app.get('/v1/movimientos',function(req, res){
  res.sendfile('movimientosv1.json');
})
//regresa un archivo json que esta almacenado en una variable
app.get('/v2/movimientos',function(req, res){
  res.json(movimientosJSON);
})
//obtener un elemento del areglo json
app.get('/v2/movimientos/:indice',function(req, res){
  console.log(req.params.indice);
  res.send(movimientosJSON[req.params.indice]);
})
//Enviar datos a partir de los query param
app.get('/v3/movimientosQuery',function(req, res){
  console.log(req.query);
  res.send("Recibo");
})
//Enviar paramatro a travez del body de la peticion
app.post('/v3/movimientos',function(req, res){
  var nuevo=req.body;
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  res.send('Movimiento dado de alta');
})
//
app.get('/clientes',function(req, res){
  var clienteMLab = requestjson.createClient(urlClientes);
  clienteMLab .get('', function(err, resM, body){
      if(err){
        cosole.log(body);
      }else{
        res.send(body);
      }
  })
});

app.post('/login',function(req, res){
  res.set("Access-Control-Allow-Headers", "Content-Type" )
  var email = req.body.email;
  var password = req.body.password;
  var query = 'q={"email":"'+email+'","password":"'+password+'"}';
  clienteMLabRaiz = requestjson.createClient(urlRaizMLab + "/Usuarios?"+apiKey + "&"+ query);
  console.log(urlRaizMLab + "/Usuarios?"+apiKey + "&" + query);
  clienteMLabRaiz.get('',function(err,resM,body){
    if(!err){
        if(body.length == 1){
          res.status(200).send('Usuario logado');
        }else {
          res.status(404).send('Usuario no encontrado, registrese');
        }
    }
  })
});

app.post('/usuarios', function(req,rest) {
  usuarioMLab.post('', req.body, function(err, resM, body) {
    if (err) {
      rest.status(400).send("Error !!!");
    }else {
      rest.status(200).send(body);
    }
  })
});

app.post('/clientes', function(req,rest) {
  usuarioMLabClientes.post('', req.body, function(err, resM, body) {
    if (err) {
      rest.status(400).send("Error !!!");
    }else {
      rest.status(200).send(body);
    }
  })
});

app.post('/alertas', function(req,rest) {
  usuarioMLabAlertas.post('', req.body, function(err, resM, body) {
    if (err) {
      rest.status(400).send("Error !!!");
    }else {
      rest.status(200).send(body);
    }
  })
});

app.get('/alertas',function(req, res){
  var clienteMLab = requestjson.createClient(urlAlertas);
  clienteMLab.get('', function(err, resM, body){
      if(err){
        cosole.log(body);
      }else{
        res.send(body);
      }
  })
});


app.get('/sepomex/:cp',function(req, res){
  console.log(req.params.cpp);
  var urlSepomex="https://api-sepomex.hckdrk.mx/query/info_cp/"+req.params.cp;
  var clienteSepomex = requestjson.createClient(urlSepomex);
  clienteSepomex.get('', function(err, resM, body){
      if(err){
        res.status(400).send("Error !!!");
      }else{
        res.status(200).send(body);
      }
  })
});
